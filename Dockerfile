FROM golang:1-alpine3.8 as builder

ARG CACHE_TAG=dev

WORKDIR /go/src/bitbucket.org/skobkin/magnetico-web-telegram-bot

ENV GOPATH=/go

# Installing build deps
RUN apk update && apk add \
    bash \
    git \
    ca-certificates

RUN go get -u github.com/golang/dep/cmd/dep

RUN git clone --depth=1 --single-branch https://skobkin@bitbucket.org/skobkin/magnetico-web-telegram-bot.git /go/src/bitbucket.org/skobkin/magnetico-web-telegram-bot \
    && dep ensure

RUN go install -ldflags "-s -w -X main.buildVersion=$CACHE_TAG -X main.buildDate=`date -u +%Y-%m-%dT%H:%M:%SZ`"

# Second stage (clean production build)
FROM alpine:3.8

WORKDIR /go/bin

# Copying service binary
COPY --from=builder /go/bin/magnetico-web-telegram-bot .
# Copy CA certs (for Telegram API)
COPY --from=builder /etc/ssl/certs /etc/ssl/certs

ENV SITE_URL="https://magnetico.pw/"
ENV API_TOKEN=""
ENV TELEGRAM_TOKEN=""
ENV SENTRY_DSN=""

ENTRYPOINT ["/go/bin/magnetico-web-telegram-bot"]
