package main

import (
	"bitbucket.org/skobkin/magnetico-web-telegram-bot/magneticoweb"
	"fmt"
	"github.com/alecthomas/kingpin"
	"github.com/getsentry/raven-go"
	"github.com/yanzay/tbot"
	"log"
	"math"
	"os"
	"strconv"
)

var client magneticoweb.ApiClient

var (
	// Build data
	buildVersion string
	buildDate    string
	// CLI params
	siteUrl       = kingpin.Flag("site-url", "Magnetico Web site URL with trailing slash").Short('s').Envar("SITE_URL").Default("https://magnetico.pw/").String()
	apiToken      = kingpin.Flag("api-token", "Magnetico Web API token").Short('a').Envar("API_TOKEN").Required().String()
	telegramToken = kingpin.Flag("tg-token", "Telegram Bot API token").Short('t').Envar("TELEGRAM_TOKEN").Required().String()
)

func main() {
	kingpin.Parse()

	log.Printf("MagneticoWeb Telegram Bot version %s (%s)", buildVersion, buildDate)

	raven.SetDSN(os.Getenv("SENTRY_DSN"))

	raven.CapturePanic(func() {
		bot, err := tbot.NewServer(*telegramToken)
		if err != nil {
			raven.CaptureErrorAndWait(err, nil)
			log.Fatal(err)
		}

		client = magneticoweb.New(*siteUrl+"api/v1/", *apiToken)

		bot.HandleDefault(DefaultHandler)

		bot.HandleFunc("/s {text}", SearchCommandHandler)
		bot.HandleFunc("/help", HelpCommandHandler)

		err = bot.ListenAndServe()

		raven.CaptureErrorAndWait(err, nil)
		log.Fatal(err)
	}, nil)
}

func DefaultHandler(message *tbot.Message) {
	processSearch(message, message.Text())
}

func SearchCommandHandler(message *tbot.Message) {
	processSearch(message, message.Vars["text"])
}

func HelpCommandHandler(message *tbot.Message) {
	message.Reply("Send /help for help")
}

func processSearch(message *tbot.Message, text string) {
	const torrentTemplate string = "*%s*\n" + "[magnet](%smagnet/%s): `%s` (%s)"

	response, err := client.FindTorrents(text, 1)

	if err != nil {
		log.Println("HTTP client error: ", err)
		message.Reply("Some error happened. Try again.")

		return
	}

	log.Println(response)

	msgText := ""

	if len(response.Data.Items) > 0 {
		for _, torrent := range response.Data.Items {
			msgText += fmt.Sprintf(torrentTemplate, torrent.Name, *siteUrl, torrent.InfoHash, torrent.InfoHash, getReadableSize(torrent.TotalSize)) + "\n\n"
		}
	} else {
		msgText = "Nothing found :("
	}

	message.Reply(msgText, tbot.WithMarkdown)
}

func getReadableSize(bytes int) string {
	suffixes := [9]string{"B", "KB", "MB", "GB", "TB", "PB", "EB", "ZB", "YB"}
	const sizeDivider float64 = 1024

	bytesString := strconv.Itoa(bytes)
	factor := int(math.Floor(float64(len(bytesString)-1) / 3))
	maxSuffixIndex := len(suffixes)

	suffixIndex := 0
	if maxSuffixIndex >= factor {
		suffixIndex = factor
	} else {
		suffixIndex = maxSuffixIndex
	}

	suffix := suffixes[suffixIndex]

	return fmt.Sprintf("%.3f %s", float64(bytes)/math.Pow(sizeDivider, float64(suffixIndex)), suffix)
}
