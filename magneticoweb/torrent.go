package magneticoweb

type Torrent struct {
	DiscoveredOn int    `json:"discoveredOn"`
	Id           int    `json:"id"`
	InfoHash     string `json:"infoHash"`
	Name         string `json:"name"`
	TotalSize    int    `json:"totalSize"`
}
