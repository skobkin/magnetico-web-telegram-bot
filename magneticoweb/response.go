package magneticoweb

type Response struct {
	Code    int    `json:"code"`
	Data    Page   `json:"data"`
	Message string `json:"message"`
	Status  string `json:"status"`
}
