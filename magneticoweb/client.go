package magneticoweb

import (
	"bitbucket.org/skobkin/dumb-http-go"
	"encoding/json"
	"errors"
	"github.com/getsentry/raven-go"
	"log"
	"net/http"
	"net/url"
	"strconv"
)

// Package errors
var (
	ErrHttpRequest         = errors.New("magneticoweb-api: HTTP request error")
	ErrJsonDeserialization = errors.New("magneticoweb-api: JSON deserialization error")
	ErrAuthNeeded          = errors.New("magneticoweb-api: Authentication needed")
	ErrSomeError           = errors.New("magneticoweb-api: Some error")
)

type ApiClient struct {
	client simple_http.Client
	apiUrl string
	token  string
}

// New creates new Magnetico.PW API client instance and initialize it with provided URL
func New(apiUrl string, token string) ApiClient {
	return ApiClient{
		simple_http.Client{http.Client{}},
		apiUrl,
		token,
	}
}

func (c *ApiClient) FindTorrents(query string, pageNumber int) (Response, error) {
	if pageNumber == 0 {
		pageNumber = 1
	}

	var response Response

	if len(c.token) == 0 {
		log.Println("Can not search torrents. Auth token is needed.")

		return response, ErrAuthNeeded
	}

	data := url.Values{}
	data.Set("query", query)
	data.Set("page", strconv.Itoa(pageNumber))

	headers := map[string]string{
		"api-token": c.token,
	}

	body, reqErr := c.client.MakeGetRequest(c.apiUrl+"torrents", &data, &headers)

	if reqErr != nil {
		raven.CaptureError(reqErr, nil)

		return response, ErrHttpRequest
	}

	jsonErr := json.Unmarshal(body, &response)

	if jsonErr != nil {
		log.Println(jsonErr)
		raven.CaptureError(jsonErr, nil)

		return response, ErrJsonDeserialization
	}

	if response.Code == 401 {
		raven.CaptureMessage(response.Message, nil)

		return response, ErrAuthNeeded
	}
	if response.Code != 200 {
		return response, ErrSomeError
	}

	return response, nil
}
