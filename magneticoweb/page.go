package magneticoweb

type Page struct {
	CurrentPage     int       `json:"currentPage"`
	Items           []Torrent `json:"items"`
	MaxPerPage      int       `json:"maxPerPage"`
	NumberOfPages   int       `json:"numberOfPages"`
	NumberOfResults int       `json:"numberOfResults"`
}
